export const BOT_QUERY = async ( botElement: Element ) => getLinks( botElement );

const getLinks = ( botElement: Element ) => botElement
  ?.shadowRoot
  .querySelector( 'div > df-messenger-chat' )
  ?.shadowRoot
  .querySelector( 'div > df-message-list' )
  ?.shadowRoot
  ?.querySelector( 'div > #messageList > df-card:last-of-type' )
  ?.shadowRoot
  .querySelectorAll( 'df-button' );

export const BOT_ANCHOR_TEXT = ( botElement: Element ) => botElement
  .shadowRoot?.querySelector( 'a > #dfLinkText').innerHTML;
