import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { LoginLayoutComponent } from './login-layout.component';

const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login',
      },
      {
        path: '',
        loadChildren: () => import('./screens/login/login.module').then((m) => m.LoginModule),
      },
      {
        path: 'forgot-password',
        loadChildren: () => import('./screens/forgot-password/forgot-password.module').then((m) => m.ForgotPasswordModule),
      },
      {
        path: 'password',
        loadChildren: () => import('./screens/change-password/change-password.module').then((m) => m.ChangePasswordModule),
      }
    ],
  },
];

@NgModule({
  declarations: [LoginLayoutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LoginLayoutModule { }
