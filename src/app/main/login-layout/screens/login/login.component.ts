import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public title = 'Login'

  constructor(){}

  ngOnInit(): void{
      
  }

  showZenDesk(){
    const zendeskBtn : HTMLElement = document.querySelector('.btn-top-zendesk');
    zendeskBtn.click()    
  }

}
