import { Component, OnInit } from '@angular/core';

import { NgxZendeskWebwidgetService } from 'ngx-zendesk-webwidget';
import { ZendeskConfig } from '@shared/class/zenDesk.class';
import { ZenDeskService } from '@app-services/zenDesk/zen-desk.service';
import _get from 'lodash/get';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
   public title = 'Change Pasword';
   constructor(){}

  ngOnInit(): void{}

  showZenDesk(){
    const zendeskBtn : HTMLElement = document.querySelector('.btn-top-zendesk');
    zendeskBtn.click()    
  }  

}
