import { TestBed } from '@angular/core/testing';

import { ZenDeskService } from './zen-desk.service';

describe('ZenDeskService', () => {
  let service: ZenDeskService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZenDeskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
