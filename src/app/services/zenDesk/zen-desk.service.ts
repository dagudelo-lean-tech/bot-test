import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ZenDeskService {
  private isInitialized$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor() {}

  update(isInitialized) {
    this.isInitialized$.next(isInitialized);
  }

  isInitializedAsObsevable(): Observable<boolean> {
    return this.isInitialized$.asObservable();
  }
}
