import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { BOT_ANCHOR_TEXT, BOT_QUERY } from './queries';
import { ToolbarComponent } from '@shared/components/toolbar/toolbar.component';

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
} )
export class AppComponent implements OnInit, AfterViewInit {


  constructor() {
    AppComponent.executeScriptBot();
  }

  title = 'bot-test';

  @ViewChild( 'Bot', { static: true } ) botElement: ElementRef;
  btnSubscription: Subscription;

  @ViewChild( ToolbarComponent ) toolBarComponent: ToolbarComponent;

  private static executeScriptBot() {
    const script = document.createElement( 'script' );
    script.type = 'text/javascript';
    script.src = 'https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1';
    document.getElementsByTagName( 'head' )[0].appendChild( script );
  }

  ngOnInit(): void {
  }

  private botClick() {
    this.btnSubscription = fromEvent( this.botElement.nativeElement, 'click' ).subscribe( {

      next: async () => {

        const botButtons = await new Promise( resolve =>
          setTimeout( resolve, 500, document.getElementsByClassName( 'bot_wp' ).item( 0 ) ) );

        const links: NodeListOf<any> = await BOT_QUERY( botButtons as Element );

        links?.forEach( link => link.addEventListener( 'click', ( event ) => {
          if ( BOT_ANCHOR_TEXT( event.target ) === 'Contact Us' ) {
            this.toolBarComponent.showZenDesk();
          }
        } ) );
      }
    } );

  }

  ngAfterViewInit(): void {
    this.botClick();
  }

}
