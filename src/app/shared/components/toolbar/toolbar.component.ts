import { Component, OnInit } from '@angular/core';

import { NgxZendeskWebwidgetService } from 'ngx-zendesk-webwidget';
import { ZendeskConfig } from '@shared/class/zenDesk.class';
import { ZenDeskService } from '@app-services/zenDesk/zen-desk.service';
import _get from 'lodash/get';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(
    private _NgxZendeskWebwidgetService: NgxZendeskWebwidgetService,
    public zenDeskService: ZenDeskService
  ){}

  ngOnInit(): void{
    // zendesk
    this.loadZenDesk();
  }

  async loadZenDesk() {
    this._NgxZendeskWebwidgetService.isInitialized();
    try {
      await this._NgxZendeskWebwidgetService.initZendesk(new ZendeskConfig());
      this._NgxZendeskWebwidgetService.zE('webWidget:on', 'close', () => {
        this._NgxZendeskWebwidgetService.zE('webWidget', 'hide');
      });
      this.zenDeskService.update(true);
    } catch (error) {
      this.zenDeskService.update(false);
    }
  }

  showZenDesk() {
    this._NgxZendeskWebwidgetService.zE('webWidget', 'show');
    const iframe: HTMLIFrameElement = document.querySelector(
      '#launcher'
    ) as HTMLIFrameElement;
    if (iframe) {
      const buttonZenDesk: HTMLElement = iframe.contentDocument.querySelector(
        '#Embed .wrapper-ATBcr'
      );
      if (buttonZenDesk) {
        buttonZenDesk.click();
        const user = JSON.parse(localStorage.getItem('userData'));
        const name = _get(user, 'name', '');
        const email = _get(user, 'email', '');
        this._NgxZendeskWebwidgetService.zE('webWidget', 'prefill', {
          name: {
            value: name,
            readOnly: true,
          },
          email: {
            value: email,
            readOnly: true,
          },
        });
      }
    }
  }

}

