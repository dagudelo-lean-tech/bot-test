import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  
  {
    path: '',
    loadChildren: () => import('./main/login-layout/login-layout.module').then((m) => m.LoginLayoutModule),
  },
  { path: '404', loadChildren: () => import('./main/404/page-not-found/page-not-found.module').then((m) => m.PageNotFoundModule) },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
